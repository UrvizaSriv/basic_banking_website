<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create User</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/table.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/createuser.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">

</head>

 <!-- <body background=#000000 > -->
 <body style="background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARwAAACxCAMAAAAh3/JWAAABPlBMVEX+/tj2PmL95kCFS4h2pTv+/9v//9z/6UD+5kD3PmH96z7+/9j6PWByozuBS4n2N2Nuojv2NGN+S4r2LVv95TfSRnH2L2SBRIV9O4N/P4T8PV/lQ2vKR3XASHjqQmj2N1/97s7707z6rqT8w0z3TmL720T8yEr4f1n61EX87Gr9+Lakuz1uoi/s5svFrbGSYpHk2sa0labWxrukfpv18dHJtbOZbJR6M4GPS4WrSn+LVYraRW6eS4Pd0MG1SXuqSn67Xof5k5P3VW/3Y3f6wbD3goj3b3/4oJv738P6vK36zbj4e4X3Xl74d1z5k0j54LD6oFb4ilj3qU/88Y/2Y1z6p1T86lr6tk/89af3eVv9/Mn5lFbh2EH87GeLsj7AyD+Ztj26xUDW1EDT1lrI2pyPtlzq8cOzzYXF15mIsVF3TkkaAAAGPElEQVR4nO3di34TRRQG8Jl0S7IhtyalSaOyhaS0QgCRW0EUDXJRU21r5NKWolUR3/8FnJndZEOTbbLZPWd/n93vDebPmXPObhIqFlhTvy6QwmrTuHEu6fOGCqdN/ebi+aTPGyqcOKtfLCZ93HDhtLmFdal4Kyfps4YOH03jS7BLxYhTv72Y4gRl9U7SRw0fLpvGXbi6YcOp3xRYK44JE04DbcUx4bFZvYVow1U5SR9zvrDQAK44Jhw2asVJ+pjzhQMHccUxYbBp3EV74ByE3qZ+D3HFMaHHuX8dtOMIsbVQrZZKJTobvLc4fuyLn1z47MFWqaqNKHAenkO9VEJI2y7oXPzq028e6TKqxkt0/2vYS6VwvNgKqSndMqoaoliQ6rdxL5WPMzAqFJrN5rCMIt+0+h3gwjmJMyijgldGj6N1I8i3OH4m4vhlVGjabhmV5iGq3wPuxuJ0HL+MClKaMloIZwT3QdWJTMcZbUejZTTdCHnFMZkdxysj2yujB9PL6GHSh4uakDh+FemOrcro8VZgw24grzgm8+EMykgZ2aPdaBQJ9i2Onyg4vlHTLaNHo2WEveKYxIAzJNJTTZeR6dir2CuOSWw4vlGzoMvoW+wVxyRunCFRIemTxRAaHJXapfRaBcb5DnwDFIQ4dhf21fEwZDiy9gL+XtHhOJdTnGCcJ/BNhw5HOmtJHy5qCHFq36PfK0Ic5yn6vSLEkTLpw0UNpQ38kkyJA78kU+LY3bRygoO+JNPiXMa+V6Q4zrO0coJTS/p40UKMg70k0+KAL8m0OOBLMrEN9jAnxnGeI98rYhy7m+IEp7aW9AkjhBwH+U0yNY7zDPheUeNIuYZbOuQ2tR9TnMAgL8nkONJJcYIDvCTT4wAvyQw4T9LKCQ7uksyB8wNq6TDg4C7JDDiyhvodLxYc1CWZAwf2Y2EOHNlN+pRzhgUHdUlmwXGepzjBOKBvkllwUJdkJhzMJZkHx/kJ8l7x4NiYHwvz4IAuyUw4mEsyE46UaeUEB3JJZsNB/O4kFw7km2QuHFkD/J8U+XAAvzvJhoO4JLPhSJniBAfwB0Z8OIBLMh+O3QX7MzysPQdvSebEgVuSGXHwfoXPiIP3K3xOHLglmRMH7ruTnDjSARvmrDhoSzIrDtqSzIsD9it8TptyufYi6fOGChfL0sqK3dv++Reo0uFgWSr3NnZeba7n8/l0WnksS6pa2tu7e61NpZLPZjOZ5V/PfEM2xSLb2wd7V9ZzHoub5f2kjxsu8ZnYXmuR/Y2d1pVc/iMW1+ZzrMKJC0ffIdnu7/7WWs9lx1QGOFfPGE7Zay36DmV1sUxScZN9CWYTAUffobLde6PmUGYKi1c4r88CjimWcn93p7XptpZpLF6w5rgIi2N2OdVa9lqTGu6UwgGb42JmHDOHVtpvDl6Z1hJKZYCzD7Ud68zEstTu6xU3H+YOjdmgzXERiOMuLXo+9w/0eA57hybgXIUrnMk4Zpfrbas7FKrhnhq4OS5O4pTLK3rzN4+JM43nmYM3x8UQx9vl9Iq7GXoOzRS4Oa7y0WNihoRFB3COq5R7G3o8u62FxMXF2UfECX5MjNUGcI6rULN4OIBzXHDhIM5xwYSz/BqycJgqB3GOCx4czDkumHDwnsfdMNjkMOe4YMEBneOCBeclqg0DDuTzuBsGHNw/y0hvgzrHBQcO6hwX9Digz+NuyHFg57hguFbAhUONAzzHBTVODvV53A0tDvIcV8nl1D8vHQ7wHFc5PFTFn/WS04nT5i104YhK0VI5+v3aH+/eHWupTCY+KOg5rqJprKJKxU3ROlJSikpJHQ6gMll9+8Jb4T6Pu7HGUxxY6aI6MkV1fGxqyi+qmRoV9hwXE3EmS1Uq+vZ5UsOaMlSqa022Ov//q5zToUbu32ijMlhayXcCn+MiHM44VqV4olFpqkFJgc9xEQ1nSlH9iV44ceGMYykk6EcHHSIcnc5f6KVDiGP9neIEp/MevCOT4vwDXjqUOJYF3pJJbdBbMikOekumxelg3ytinA/QpUOLY/2b4gQHe9WhxoFedYhxrE5aOafgIK861DjQqw45Tud90kecP/Q4H3C7DjmOZeHeK3ob4FWHHsfCXXUYcKykzzh3GGxgV53/AJ8lDb8GFZPtAAAAAElFTkSuQmCC');  background-attachment: fixed; background-repeat: no-repeat; background-size: cover;">
<?php
    include 'config.php';
    if(isset($_POST['submit'])){
    $name=$_POST['name'];
    $email=$_POST['email'];
    $balance=$_POST['balance'];
    $sql="insert into users(name,email,balance) values('{$name}','{$email}','{$balance}')";
    $result=mysqli_query($conn,$sql);
    if($result){
               echo "<script> alert('Hurray! User created');
                               window.location='transfermoney.php';
                     </script>";
                    
    }
  }
?>

<?php
  include 'navbar.php';
?>

<h2 class="text-center pt-4" style = "color : #000000;   "><b><u><em>Indian Bank</em></u></b></h2>
        <br>
        <h2 class="text-center pt-4"><b>Create a User</b></h2>
        <br>

  <div class="background" >
  <div class="container">
    <div class="screen" >
      </div>
      
        <div class="screen-body-item" >
          <form class="app-form" method="post" >
            <div class="app-form-group">
              <input class="app-form-control" placeholder="NAME" type="text" name="name" required>
            </div>
            <div class="app-form-group">
              <input class="app-form-control transparent-form" placeholder="EMAIL" type="email" name="email" required>
            </div>
            <div class="app-form-group">
              <input class="app-form-control" placeholder="BALANCE" type="number" name="balance" required>
            </div>
            <br>
            <div class="app-form-group button">
              <input class="app-form-button" style="margin-bottom: 260px;" type="submit" value="CREATE" name="submit"></input>
              <input class="app-form-button" type="reset" value="RESET" name="reset"></input>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html> 