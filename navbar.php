<!-- navbar --> 
      <nav class="navbar navbar-expand-md navbar-light bg-light">
      <!-- <a class="navbar-brand" href="index.php"><b>Indian Bank</b></a> -->
      
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Home <i class="fa fa-home" style="font-size:24px"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="createuser.php">Create User <i class="fa fa-male" style="font-size:24px"></i><i class="fa fa-female" style="font-size:24px"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="transfermoney.php">Transfer Money <i class="fa fa-arrow-circle-down" style="font-size:24px"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="transactionhistory.php">Transaction History <i class="fa fa-history" style="font-size:24px"></i></a>
              </li>
          </div>
       </nav>
